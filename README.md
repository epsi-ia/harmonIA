# Quickstart
- `cd harmonIA`
- `python lm.py --data_path=./Nottingham --model=test`

If you need to save your learning model :
- `python lm.py --data_path=./Nottingham --model=test --save_path=./your/beautiful/path`

# Options

- model
  - test
  - small


# Based on TensorFlow tutorial
# See https://github.com/tensorflow/models/blob/master/tutorials/rnn/ptb/reader.py
# =================================================================================

import tensorflow as tf
import sys, os
import collections
from mido import MidiFile

def get_raw_track(midi_file):
	mid = MidiFile(midi_file)
	tracks = mid.tracks

	if len(tracks) < 3:
		print("Unparsable track with {} tracks. Expected tracks : 3"
			.format(len(tracks)))
		return metadata, None, None

	metadata = tracks[0]
	melody = tracks[1]
	harmony = tracks[2]

	return metadata, melody, harmony


def get_note_id(notes):
	"""Concat notes from a note record
	to have a string identifier.

	Examples:
		- melody : {"note": [56], ...} will produce '56'
		- harmony : {"note": [56, 76, 78], ...} will produce '56_76_78'
	"""
	return "_".join(map(str, notes))


def get_raw_notes(track):
	raw_notes = []
	notes = []
	time_on = 0
	time_off = 0
	note_on = 0
	note_off = 0
	velocity = 0

	for msg in track:
		if msg.type == 'note_on':
			time_on += msg.time
			note_on += 1
			notes.append(msg.note)
			velocity = msg.velocity

		if msg.type == 'note_off':
				time_off += msg.time
				note_off += 1

		if note_on == note_off and not note_off == 0:
			raw_notes.append({
				'notes': notes,
				'id': get_note_id(notes),
				'velocity' : velocity,
				'time_length': abs(time_off - time_on),
				'time_start' : time_on
			})

			time_on = 0
			time_off = 0
			note_on = 0
			note_off = 0
			notes = []

	return raw_notes


def build_notes_id(raw_notes):
	# set unique by id (compositon of notes)
	unique_notes = list({raw['id']:raw for raw in raw_notes}.values())
	notes_id = []

	for note in raw_notes:
		# one or many occurences can be removed because unique_notes array
		# contains only unique ids, where an id is one(melody) or many (harmony) notes
		if note in unique_notes:
			notes_id.append(unique_notes.index(note))

	return notes_id



def midi_raw_data(data_path=None):
	"""Load MIDI raw data from data directory 'data_path'

	Read MIDI files, get notes from tracks and performs
	mini-batching of the inputs.

	Args:
		data_path: string path to the directory where MIDI files
		has been extracted.

	Returns:
		tuple (train_data, valid_data, test_data, notes)
	"""

	train_path = os.path.join(data_path, "train/ashover_simple_chords_1.mid")
	valid_path = os.path.join(data_path, "valid/ashover_simple_chords_10.mid")
	test_path = os.path.join(data_path, "test/ashover_simple_chords_11.mid")

	metadata_t, melody_t, harmony_t = get_raw_track(train_path)

	if (melody_t is None or harmony_t is None):
		print("Unparsable track metadata : {}"
			.format(metadata_t))

	raw_melody = get_raw_notes(melody_t)
	raw_harmony = get_raw_notes(harmony_t)
	raw_notes = raw_melody + raw_harmony
	notes_to_id = build_notes_id(raw_notes)

	return notes_to_id, [], [], len(notes_to_id)

def producer(raw_data, batch_size, num_steps, name=None):
	"""Iterate on the raw MIDI data.

	This chunks up raw_data into batches of examples and returns Tensors that
	are drawn from these batches.

	Args:
		raw_data: one of the raw data outputs from ptb_raw_data.
		batch_size: int, the batch size.
		num_steps: int, the number of unrolls.
		name: the name of this operation (optional).

	Returns:
		A pair of Tensors, each shaped [batch_size, num_steps]. The second element
		of the tuple is the same data time-shifted to the right by one.

	Raises:
		tf.errors.InvalidArgumentError: if batch_size or num_steps are too high.
	"""
	with tf.name_scope(name, "PTBProducer", [raw_data, batch_size, num_steps]):
		print("data_len : " + str(len(raw_data)))
		print("batch_len : " + str(len(raw_data) // batch_size))
		raw_data = tf.convert_to_tensor(raw_data, name="raw_data", dtype=tf.int32)
		data_len = tf.size(raw_data)
		batch_len = data_len // batch_size
		data = tf.reshape(raw_data[0 : batch_size * batch_len],
									[batch_size, batch_len])

		epoch_size = (batch_len - 1) // num_steps
		assertion = tf.assert_positive(
			epoch_size,
			message="epoch_size == 0, decrease batch_size or num_steps"
		)

		with tf.control_dependencies([assertion]):
			epoch_size = tf.identity(epoch_size, name="epoch_size")

		i = tf.train.range_input_producer(epoch_size, shuffle=False).dequeue()
		x = tf.strided_slice(data, [0, i * num_steps],
										 [batch_size, (i + 1) * num_steps])
		x.set_shape([batch_size, num_steps])
		y = tf.strided_slice(data, [0, i * num_steps + 1],
										 [batch_size, (i + 1) * num_steps + 1])
		y.set_shape([batch_size, num_steps])

		return x, y
